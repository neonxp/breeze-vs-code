# Change Log

All notable changes to the "breeze-vs-code" extension will be documented in this file.

Check [Keep a Changelog](http://keepachangelog.com/) for recommendations on how to structure this file.

## [Unreleased]

## [0.0.4] - 2020-03-25

- dark: syntax colors

## [0.0.3] - 2020-03-20

- dark: scrollbar colors
- dark: border colors
- dark: list item selection colors

## [0.0.2] - 2020-03-19

- rename package
- improve dark theme

## [0.0.1] - 2020-03-18

- basic dark theme

[Unreleased]: https://invent.kde.org/guoyunhe/breeze-vs-code/compare/v0.0.4...HEAD
[0.0.4]: https://invent.kde.org/guoyunhe/breeze-vs-code/compare/v0.0.3...v0.0.4
[0.0.3]: https://invent.kde.org/guoyunhe/breeze-vs-code/compare/v0.0.2...v0.0.3
[0.0.2]: https://invent.kde.org/guoyunhe/breeze-vs-code/compare/v0.0.1...v0.0.2
[0.0.1]: https://invent.kde.org/guoyunhe/breeze-vs-code/releases/tag/v0.0.1
