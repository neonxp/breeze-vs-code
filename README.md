# Breeze VS Code Theme

<img src="https://invent.kde.org/guoyunhe/breeze-vs-code/-/raw/master/screenshot-dark.png">

* https://code.visualstudio.com/api/references/theme-color
* https://hig.kde.org/style/color/dark.html
